#!/bin/bash

sudo apt-get remove -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
sudo apt-key del "$(sudo apt-key list | grep "Docker Release" -B 1 | grep -E "^\s+(([0-9A-Z]{4})+).*$" | sed -e 's/^[ \t]*//')"
sudo rm /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get remove -y docker-ce docker-compose
