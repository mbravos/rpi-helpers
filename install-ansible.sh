#!/bin/bash

ansible_version=${1:-"2.5.0"}

if [ -z $ansible_version ]; then
	echo "ERROR: Ansible verison to install is not defined"
	exit -1
fi

echo "Ansible $ansible_version will be installed"

sudo apt-get update && \
    sudo apt-get -y install \
      gcc \
      musl-dev \
      libffi-dev \
      libssl-dev \
      python-dev

sudo apt-get -y install \
      bash \
      curl \
      tar \
      openssh-client \
      sshpass \
      git \
      python \
      python-boto \
      python-dateutil \
      python-httplib2 \
      python-jinja2 \
      python-paramiko \
      python-pip \
      python-yaml \
      ca-certificates

sudo pip install docker-py
sudo pip install ansible==$ansible_version

sudo mkdir -p /etc/ansible /ansible
echo "[local]" | sudo tee --append /etc/ansible/hosts 
echo "localhost" | sudo tee --append /etc/ansible/hosts
